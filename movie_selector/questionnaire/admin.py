from django.contrib import admin
from questionnaire.models import *


admin.site.register(Movie)
admin.site.register(Genre)
admin.site.register(Director)
admin.site.register(ScriptAuthor)
admin.site.register(Actor)

