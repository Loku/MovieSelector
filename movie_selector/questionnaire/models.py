from django.db import models


class Genre(models.Model):
    name = models.TextField(null=False)

    def __str__(self):
        return self.name


class Director(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class ScriptAuthor(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class Actor(models.Model):
    name = models.TextField()

    def __str__(self):
        return self.name


class Movie(models.Model):
    name = models.TextField(null=False)
    duration = models.IntegerField(null=True)
    genre = models.ForeignKey(Genre)
    created_year = models.IntegerField(null=False)
    director = models.ForeignKey(Director)
    script_author = models.ForeignKey(ScriptAuthor)
    production = models.TextField()
    main_actor = models.ForeignKey(Actor)

    def __str__(self):
        return self.name
